/* eslint-disable  */

import React from 'react';
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import App from './App';
import { name as appName } from './app.json';
import {  persistor, store } from './src/redux/Store';
import { PersistGate } from 'redux-persist/integration/react';

const RNRedux = () => {
    return (<Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <App />
        </PersistGate>
    </Provider>);
}

AppRegistry.registerComponent(appName, () => RNRedux);
