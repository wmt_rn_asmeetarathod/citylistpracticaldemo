/* eslint-disable  */
import React, { useEffect } from 'react';
import type { Node } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Routes from './src/router/routes';

import AppRoot from './src/router/index';
import { StatusBar } from 'react-native';

const Stack = createStackNavigator();

const App = (props) => {

  // const [selected,setSelected]=useState(false);
  // console.log("App Props ==>> ", props);

  // useEffect(() => {
  //   const selectedFilteredList =  props.cityList.filter((city)=> city.isSelected === true)
  //   console.log("selectedFilteredList ==>> ",selectedFilteredList);

  //   if(selectedFilteredList.length >0){
  //     setSelected(true)
  //   }
  //   else{
  //     setSelected(false)
  //   }
  //   console.log("selected ==>> ", selected);
    
  // }, [props.cityList,selected]);

  // const handleMultipleDelete = () =>{
  //   props.deleteMultipleCity();
  // }
  return (
    <>
      <NavigationContainer >
      <StatusBar barStyle="light-content" backgroundColor='#6C3483' />
        <Stack.Navigator initialRouteName={Routes.AppRoot}>
          <Stack.Screen name={Routes.AppRoot} component={AppRoot} options={{ headerShown: false }} />
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
};


export default App;
