/* eslint-disable */

import validatejs from 'validate.js';

export function validate(fieldName, value) {

  const formValues = {}
  formValues[fieldName] = value;

  const formFields = {}
  formFields[fieldName] = validation[fieldName];

  const result = validatejs(formValues, formFields);

  if (result) {
    return result[fieldName][0];
  }
  return null;
}

export const validation = {
  cityName: {
    presence: {
      
      message: "is required"
    },
    type: "string",
    length: {
      minimum: 1,
      message: "^City Name is must be atleast 1 latter"
    }
  },
  latitude: {
    presence:
      true
    ,
    numericality: {
      onlyInteger: true,
      lessThan: 90,
      greaterThan: -90,
      message: "Latitude Value must be between -90 & 90"
    }
  },
  longitude: {
    presence: true
    ,
    numericality: {
      onlyInteger: true,
      lessThan: 180,
      greaterThan: -180,
      message: "Longitude Value must be between -180 & 180"
    }
  },

}


