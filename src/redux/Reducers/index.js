/* eslint-disable  */
import { ADD_CITY, DELETE_CITY, DELETE_MULTIPLE_CITY, EDIT_CITY, FETCH_CITY_LIST, SEARCH_CITY, SELECT_CITY, UPDATE_CITY } from "../Actions";

const REHYDRATE = "persist/REHYDRATE";

const initialState = {
    cityData: [],
    editCityData: {},
    searchCityData: [],
}

export default addCityReducer = (state = initialState, action) => {
    console.log("state ==>> ", state);
    console.log("action ==>> ", action);
    switch (action.type) {
        case ADD_CITY:
            // {...state,cityData:[...state.cityData,action.payload]}
            console.log("action.payload ==>> ", action.payload);
            return { ...state, cityData: [...state.cityData, action.payload] };

        case DELETE_CITY:
            console.log("action.payload(DELETE) ==>> ", action.payload);
            const filteredCityList = state.cityData.filter((city) => city.cityName !== action.payload)
            console.log("filteredCityList ==>> ", filteredCityList);
            return { ...state, cityData: filteredCityList };

        case EDIT_CITY:
            console.log("action.payload(EDIT) ==>> ", action.payload);
            return { ...state, editCityData: action.payload };

        case UPDATE_CITY:
            console.log("action.payload(UPDATE) ==>> ", action.payload);
            const updatedCityList = state.cityData.map((city) => {
                if (city.cityName === state.editCityData.cityName) {
                    console.log("city.cityName ==>> ", city.cityName);
                    console.log("state.editCityData.cityName ==>> ", state.editCityData.cityName);
                    return city = action.payload;
                    // console.log("UPDATED city OBJECT ==>> ",city);
                }
                else {
                    return city;
                }
            })
            console.log("updatedCityList ==>> ", updatedCityList);
            return { ...state, cityData: updatedCityList };

        case SEARCH_CITY:
            console.log("SEARCH_CITY action.payload ==>> ", action.payload);
            const copyCityData = state.cityData;
            const filteredData = copyCityData.filter(obj => {
                return obj.cityName.toLowerCase().includes(action.payload.toLowerCase());
            });
            console.log("filteredData ==>> ", filteredData);
            return { ...state, searchCityData: filteredData };

        case SELECT_CITY:
            console.log("SELECT_CITY action.payload ==>> ", action.payload);
            const isSelectedUpdatedList = state.cityData.map((city) => {
                if (city.cityName === action.payload.cityName) {
                    city.isSelected = !(city.isSelected);

                }
                return city;
            })
            console.log("isSelectedUpdatedList ==>> ", isSelectedUpdatedList);

            return { ...state, cityData: isSelectedUpdatedList };

        case DELETE_MULTIPLE_CITY:
            
            console.log("case DELETE_MULTIPLE_CITY", state.cityData);

            const multipleCityDeletedList = state.cityData.filter((city) => city.isSelected !== true);
            console.log("multipleCityDeletedList ==>> ", multipleCityDeletedList);
            return { ...state, cityData: multipleCityDeletedList };

        case REHYDRATE:
            return { ...state, ...action.payload };
        default:
            return state;
    }
}