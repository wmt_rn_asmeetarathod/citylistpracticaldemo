/* eslint-disable  */

import { applyMiddleware, createStore } from "redux";
import persistReducer from "redux-persist/es/persistReducer";
import persistStore from "redux-persist/es/persistStore";
// import storage from 'redux-persist/lib/storage';
import AsyncStorage from "@react-native-community/async-storage";
import Reducers from "../Reducers";
import { createLogger } from 'redux-logger';

const persistConfig={
  key:'root',
  storage:AsyncStorage,
  whitelist: ['cityData'],
};

const pReducer=persistReducer(persistConfig,Reducers);


const store=createStore(pReducer,applyMiddleware(
  createLogger(),
));
const persistor=persistStore(store);
export {persistor,store};
