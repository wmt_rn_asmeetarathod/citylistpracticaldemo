/* eslint-disable  */

export const ADD_CITY="ADD_CITY";
export const DELETE_CITY= "DELETE_CITY";
export const FETCH_CITY_LIST="FETCH_CITY_LIST";
export const EDIT_CITY="EDIT_CITY";
export const UPDATE_CITY="UPDATE_CITY";
export const SEARCH_CITY = "SEARCH_CITY";
export const SELECT_CITY="SELECT_CITY";
export const DELETE_MULTIPLE_CITY="DELETE_MULTIPLE_CITY";

export const addCity = (city) =>{
    console.log("city ==>> ",city);
    return(
        {
            type: ADD_CITY,
            payload:city
        }
    );
}

export const deleteCity = (cityName) =>{
    console.log("(deleteCity)city name ==>> ",cityName);
    return(
        {
            type: DELETE_CITY,
            payload:cityName
        }
    )
}

export const editCity = (city) =>{
    console.log("(EDIT) city ==>> ",city);
    return (
        {
            type:EDIT_CITY,
            payload: city
        }
    )
}

export const updateCity = (city) => {
    console.log("(UpdateCity) city ==>> ",city);
    return(
        {
            type: UPDATE_CITY,
            payload: city
        }
    )
}

export const searchCity = (searchText) =>{
    console.log("searchCity searchText ==>> ",searchText);
    return(
        {
            type: SEARCH_CITY,
            payload: searchText
        }
    )
}

export const selectCity = (city) =>{
    console.log("selectCity ==>> ",city);
    return(
        {
            type: SELECT_CITY,
            payload:city
        }
    )
}

export const deleteMultipleCity = () =>{
    console.log("In deleteMultipleCity()");
    return(
        {
            type: DELETE_MULTIPLE_CITY,
            
        }
    )
}