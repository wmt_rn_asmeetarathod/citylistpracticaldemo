/* eslint-disable  */
import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import AuthenticatedStack from './authenticated';
import Routes from "./routes";

const Stack=createStackNavigator();
const AppRoot = () =>{
    return(
        <Stack.Navigator initialRouteName={Routes.Authenticated}>
          <Stack.Screen name={Routes.Authenticated} component={AuthenticatedStack}  options={{ headerShown: false }}/>
        </Stack.Navigator>
    );
}

export default AppRoot;