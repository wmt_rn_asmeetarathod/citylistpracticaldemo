/* eslint-disable  */

import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import Routes from "./routes";
import { connect } from "react-redux";
import IonIcon from 'react-native-vector-icons/dist/Ionicons';
import { useState, useEffect } from 'react';
import AddCityForm from '../screens/AddCityForm';
import EditCityForm from '../screens/EditCityForm';
import CityList from '../screens/CityList';
import { deleteMultipleCity } from '../redux/Actions';
import { View } from "react-native";
import Ripple from 'react-native-material-ripple';

const authenticatedStack = createStackNavigator();

const AuthenticatedStack = (props) => {
  const [selected, setSelected] = useState(false);
  console.log("App Props ==>> ", props);

  useEffect(() => {
    const selectedFilteredList = props.cityList.filter((city) => city.isSelected === true)
    console.log("selectedFilteredList ==>> ", selectedFilteredList);

    if (selectedFilteredList.length > 0) {
      setSelected(true)
    }
    else {
      setSelected(false)
    }
    console.log("selected ==>> ", selected);

  }, [props.cityList, selected]);

  const handleMultipleDelete = () => {
    props.deleteMultipleCity();
  }

  return (
    <authenticatedStack.Navigator initialRouteName={Routes.CityList}>
      <authenticatedStack.Screen name={Routes.CityList} component={CityList}
        options={({ route, navigation }) => (
          {
            headerRight: () => {
              return (<View style={{ display: 'flex', width:'100%',flexDirection: 'row',justifyContent:'space-between',alignItems:'center' }}>
              <Ripple rippleSize={80} disabled={selected ? true : false} rippleContainerBorderRadius={70} onPress={() => handleMultipleDelete()}>
                <IonIcon  name={selected ? "trash" : null} color="white" size={25}  />
              </Ripple>
              <Ripple rippleSize={80} rippleContainerBorderRadius={70} onPress={() => navigation.navigate(Routes.AddCityForm)} >
                <IonIcon  name="add-circle-outline" color="white" size={25} />
              </Ripple>
              </View>
              );
            },
            headerRightContainerStyle: {
              margin:5,
              padding:10,
              width:'20%' ,
              
            },
            
            headerStyle: {

              backgroundColor: '#8E44AD',
            },
            headerTintColor: 'white',

          }
        )} />
      <authenticatedStack.Screen name={Routes.AddCityForm} component={AddCityForm} options={
        {
          title: 'Add City Details',
          headerStyle: {

            backgroundColor: '#8E44AD',
          },
          headerTintColor: 'white',

        }
      } />
      <authenticatedStack.Screen name={Routes.EditCityForm} component={EditCityForm} options={
        {
          title: 'Edit City Details',
          headerStyle: {
            backgroundColor: '#8E44AD',
          },
          headerTintColor: 'white',
        }
      } />
    </authenticatedStack.Navigator>
  )
}
const mapStateToProps = (state) => {
  console.log("Authenticated state ==>> ", state);
  return { cityList: state.cityData };
}

export default connect(mapStateToProps, { deleteMultipleCity })(AuthenticatedStack);
