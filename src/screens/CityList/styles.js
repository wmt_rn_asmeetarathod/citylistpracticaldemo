/* eslint-disable  */

import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    mainContainer: {
        display: 'flex',
        flex:1,
        padding: 10,
        backgroundColor: '#E5E8E8',
        
    },
    flatListStyle:{
        flex:1,
    },
    searchBar: {
        borderRadius: 50,
        
        
        margin: 10,
        padding: 10,
        backgroundColor:'white'
    },
    vwDetail:{
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-around',
        alignItems:'center',
        flex:1,
        width:'60%',
    },
    itemStyle: {
        display: 'flex',
        padding:10,
        flexDirection: 'row',
        margin:5,
        borderRadius:10,
        justifyContent:'center',
        alignItems:'center',
        alignContent:'center',
    },
    item1:{
        display:'flex',
        alignItems:'flex-start',
        flexGrow:1,
        width:'20%',
        justifyContent:'flex-start',
    },
    item2:{
        display:'flex',
        alignItems:'center',
        flexGrow:1,
        width:'20%',
        justifyContent:'flex-start',
        
        
    },
    item3:{
        display:'flex',
        flexGrow:1,
        alignItems:'flex-end',
        alignContent:'flex-start',
        marginEnd:20,
        width:'20%',
        justifyContent:'flex-start',
    },
    iconsStyle:{
        display:'flex',
        flexDirection:'row',
        width:'20%',
    },
    dataStyle:{
        fontSize:15,
        textAlign:'left',
    }
})
export default styles;