/* eslint-disable  */

import { Alert, Button, FlatList, Text, TextInput, View, StyleSheet, TouchableOpacity } from "react-native";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { deleteCity, editCity, fetchCityList, searchCity, selectCity } from "../../redux/Actions";
import IonIcon from 'react-native-vector-icons/dist/Ionicons';
import Routes from "../../router/routes";
import styles from "./styles";
import Ripple from 'react-native-material-ripple';

const CityList = (props) => {
    const [selected, setSelected] = useState(false);
    const [filteredData, setFilteredData] = useState([]);
    const [searchText, setSearchText] = useState('');
    console.log("cityList Props ==>> ", props);

    useEffect(() => {
        console.log("(useEffect)filteredData ==>> ", filteredData);
    }, [searchText, filteredData]);

    useEffect(() => {
        const selectedFilteredList = props.cityList.filter((city) => city.isSelected === true)
        console.log("selectedFilteredList ==>> ", selectedFilteredList);

        if (selectedFilteredList.length > 0) {
            setSelected(true)
        }
        else {
            setSelected(false)
        }
        console.log("selected ==>> ", selected);

    }, [props.cityList, selected]);
    let data = props.cityList;
    if (searchText === '') {
        data = props.cityList;
    }
    else {
        data = filteredData;
    }

    const handleOnEdit = (city) => {

        console.log("city in handleOnEDIT() ==>> ", city);
        props.editCity(city);
        props.navigation.navigate(Routes.EditCityForm);

    }

    const handleOnDelete = (cityName) => {
        console.log("cityName in Del() ==>> ", cityName);
        props.deleteCity(cityName);
        Alert.alert('Delete Pressed!');
    }

    const handleLongPress = (city) => {
        Alert.alert("Selected ", city.cityName);
        props.selectCity(city);
    }

    const RenderItemCompo = ({ itemData }) => {

        return (
            <TouchableOpacity
                key={itemData.cityName}
                style={[styles.itemStyle
                    , { backgroundColor: itemData.isSelected ? "#C39BD3" : "white" }]}
                onPress={() => {
                    if (selected)
                        handleLongPress(itemData)
                }}
                onLongPress={() => {
                    if (!selected)
                        handleLongPress(itemData)
                }}
                delayLongPress={100}
            >
                <IonIcon style={{ margin: 8 }} name="checkmark-circle" color={itemData.isSelected ? "#8E44AD" : 'white'} size={20} onPress={() => handleOnEdit(itemData)} />
                <View style={styles.vwDetail}>

                    <View style={styles.item1}>
                        <Text style={styles.dataStyle}>{itemData.cityName}</Text>
                    </View>
                    <View style={styles.item2}>
                        <Text style={styles.dataStyle}>{itemData.latitude}</Text>
                    </View>
                    <View style={styles.item3}>
                        <Text style={styles.dataStyle}>{itemData.longitude}</Text>
                    </View>

                </View>
                <View style={styles.iconsStyle}>
                    <Ripple rippleSize={80} disabled={selected ? true : false} rippleContainerBorderRadius={70} onPress={() => handleOnEdit(itemData)}>
                        <IonIcon style={{ margin: 5 }} name="create" color={itemData.isSelected ? "gray" : "black" } size={20}  />
                    </Ripple>
                    <Ripple rippleSize={80} rippleContainerBorderRadius={70}>
                    <IonIcon style={{ margin: 5 }} name="trash" color="black" size={20} onPress={() => handleOnDelete(itemData.cityName)} />
                    </Ripple>
                </View>
            </TouchableOpacity>
        );

    }
    console.log("data ==>> ", data);

    const handleSearchChange = (text) => {
        setSearchText(text);
        console.log("searchText ==>> ", searchText);
        const list = props.cityList.filter(obj => {
            return obj.cityName.toLowerCase().includes(searchText.toLowerCase());
        });
        setFilteredData(list);
        console.log("filteredData ==>> ", filteredData);
        // props.searchCity(searchText);
    }
    return (

        <View style={styles.mainContainer}>

            <TextInput
                style={styles.searchBar}
                placeholder="Search here..."
                value={searchText}
                onChangeText={(text) => handleSearchChange(text)}
            />

            <View style={styles.flatListStyle}>
                <FlatList
                    data={data}
                    renderItem={({ item }) => (<RenderItemCompo itemData={item} />)}
                    keyExtractor={item => item.cityName}

                />
            </View>
        </View>
    );
}

const mapStateToProps = (state) => {
    console.log("cityList state ==>> ", state);
    return { cityList: state.cityData, searchedCityList: state.searchCityData };
}



export default connect(
    mapStateToProps,
    { deleteCity, editCity, selectCity })
    (CityList);