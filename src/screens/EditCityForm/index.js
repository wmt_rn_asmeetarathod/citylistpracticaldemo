/* eslint-disable  */


import AsyncStorage from "@react-native-community/async-storage";
import React, { useRef, useState } from "react";
import { Button, Text, StyleSheet, TextInput, View, Alert } from "react-native";
import { connect } from "react-redux";
import { addCity, updateCity } from "../../redux/Actions";
import Routes from "../../router/routes";
import styles from "./styles";
import {  validate } from '../../utils/validationUtils';


export const STORAGE_KEY = "saveCityData";
const EditCityForm = (props) => {

    const refCityName = useRef();
    const refLatitude = useRef();
    const refLongitude = useRef();
    console.log("EditCityForm props ==>> ", props);

    const [cityName, setCityName] = useState(props.editCityData.cityName);
    const [cityNameError, setCityNameError] = useState("");
    
    const [latitude, setLatitude] = useState(props.editCityData.latitude);
    const [latitudeError, setLatitudeError] = useState("");

    const [longitude, setLongitude] = useState(props.editCityData.longitude);
    const [longitudeError, setLongitudeError] = useState("");

    const validateCityData = () => {

        const cityNameError = validate("cityName", cityName);
        const latitudeError = validate("latitude", latitude);
        const longitudeError = validate("longitude", longitude);

        setCityNameError(cityNameError);
        setLatitudeError(latitudeError);
        setLongitudeError(longitudeError)


        if (!cityNameError && !latitudeError && !longitudeError) {
            handleEdit();
        }
    }
    const handleEdit = () => {

        console.log("(UPDATED) City Name ==>> ", cityName);
        console.log("(UPDATED) Latitude ==>> ", latitude);
        console.log("(UPDATED) Longitude ==>> ", longitude);
        props.updateCity({ cityName, latitude, longitude,isSelected:false });
        props.navigation.navigate(Routes.CityList);

    }


    return (
        <View style={styles.mainContainer}>
            
            <View style={styles.field}>
                <Text style={styles.titleStyle}>City Name </Text>
                <TextInput
                    ref={refCityName}
                    style={styles.textStyle}
                    placeholder="Enter City Name"
                    value={cityName}
                    onChangeText={(text) => setCityName(text)} />
                    {cityNameError ? <Text style={styles.errorText}>{cityNameError}</Text> : null}
            </View>
            <View style={styles.field}>
                <Text style={styles.titleStyle}>Latitude </Text>
                <TextInput
                    ref={refLatitude}
                    style={styles.textStyle}
                    placeholder="Enter Latitude"
                    value={latitude}
                    onChangeText={(text) => setLatitude(text)}
                />
                {latitudeError ? <Text style={styles.errorText} >{latitudeError}</Text> : null}
            </View>
            <View style={styles.field}>
                <Text style={styles.titleStyle}>Longitude </Text>
                <TextInput
                    ref={refLongitude}
                    style={styles.textStyle}
                    placeholder="Enter Longitude"
                    value={longitude}
                    onChangeText={(text) => setLongitude(text)}
                />
                {longitudeError ? <Text style={styles.errorText}>{longitudeError}</Text> : null}
            </View>
            <Button title="EDIT" onPress={() => validateCityData()} />
        </View>
    );
}


const mapStateToProps = (state) => {
    console.log("editCityForm state ==>> ", state);
    return { editCityData: state.editCityData };
}

export default connect(mapStateToProps, { updateCity })(EditCityForm);