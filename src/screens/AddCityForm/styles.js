/* eslint-disable  */

import { StyleSheet } from "react-native";

export default styles=StyleSheet.create({
    mainContainer: {
        display: 'flex',
        margin: 10,
        padding: 10,
        flex:1,
        
    },
    errorText:{
        color:'red',
        margin:5,
    },
    titleStyle:{
        fontSize:14,
        margin:5,
    },
    textStyle:{
        fontSize:16,
        borderColor:'gray',
        borderWidth:1,
        borderRadius:10,
        margin:5,
        padding:10,
    },
    
    btnAddStyle:{
        margin:20,
        fontSize:16,
        backgroundColor:"#8E44AD",
    }
})

