/* eslint-disable  */


import AsyncStorage from "@react-native-community/async-storage";
import React, { useRef, useState } from "react";
import { Button, Text, StyleSheet, TextInput, View, Alert } from "react-native";
import { connect } from "react-redux";
import { addCity } from "../../redux/Actions";
import Routes from "../../router/routes";
import { equalityCheck, validate } from '../../utils/validationUtils';
import styles from "./styles";



export const STORAGE_KEY = "saveCityData";

const AddCityForm = (props) => {
    const refCityName = useRef();
    const refLatitude = useRef();
    const refLongitude = useRef();

    const [cityName, setCityName] = useState("");
    const [cityNameError, setCityNameError] = useState("");

    const [latitude, setLatitude] = useState("");
    const [latitudeError, setLatitudeError] = useState("");

    const [longitude, setLongitude] = useState("");
    const [longitudeError, setLongitudeError] = useState("");

    const validateCityData = () => {

        const cityNameError = validate("cityName", cityName);
        const latitudeError = validate("latitude", latitude);
        const longitudeError = validate("longitude", longitude);

        setCityNameError(cityNameError);
        setLatitudeError(latitudeError);
        setLongitudeError(longitudeError)


        if (!cityNameError && !latitudeError && !longitudeError) {
            handleOnAdd();
        }
    }

    const handleOnAdd = () => {
        console.log("City Name ==>> ", cityName);
        console.log("Latitude ==>> ", latitude);
        console.log("Longitude ==>> ", longitude);

        console.log("props.cityList.length > 0 ==>> ", props.cityList.length > 0);
        if (props.cityList.length > 0) {

            if (checkCityName(cityName)) {
                Alert.alert("City Name Already Exist!");
            }
            else {
                props.addCity({ cityName, latitude, longitude, isSelected: false });
            }
        }
        else {
            props.addCity({ cityName, latitude, longitude });
        }
        props.navigation.navigate(Routes.CityList);
    }
    const checkCityName = (nameOfCity) => {
        console.log("cityName in check() ==>> ", nameOfCity);
        console.log("length ==>> ", props.cityList.length);
        let isExist = false;
        props.cityList.map((city) => {
            isExist = (city.cityName === nameOfCity) ? true : false;
            console.log("city.cityName===nameOfCity ==>> ", city.cityName === nameOfCity);
            console.log("city.cityName ==>> ", city.cityName);
            console.log("isExist ==>> ", isExist);
        })
        return isExist;
    }

    return (
        <View style={styles.mainContainer}>
           
            <View style={styles.field}>
                <Text style={styles.titleStyle}>City Name </Text>
                <TextInput
                    style={styles.textStyle}
                    placeholder="Enter City Name"
                    ref={refCityName}
                    value={cityName}
                    onChangeText={(text) => setCityName(text)} />
                {cityNameError ? <Text style={styles.errorText}>{cityNameError}</Text> : null}
            </View>
            <View style={styles.field}> 
                <Text style={styles.titleStyle}>Latitude </Text>
                <TextInput
                    style={styles.textStyle}
                    placeholder="Enter Latitude"
                    ref={refLatitude}
                    value={latitude}
                    onChangeText={(text) => setLatitude(text)}
                />
                {latitudeError ? <Text style={styles.errorText} >{latitudeError}</Text> : null}
            </View>
            <View style={styles.field}>
                <Text style={styles.titleStyle}>Longitude </Text>
                <TextInput
                    placeholder="Enter Longitude"
                    style={styles.textStyle}
                    ref={refLongitude}
                    value={longitude}
                    onChangeText={(text) => setLongitude(text)}
                />
                {longitudeError ? <Text style={styles.errorText}>{longitudeError}</Text> : null}
            </View>
            <Button style={styles.btnAddStyle} title="ADD" onPress={() => validateCityData()} />


        </View>
    );
}



const mapStateToProps = (state) => {
    console.log("cityForm state ==>> ", state.cityData);
    return { cityList: state.cityData };
}

export default connect(mapStateToProps, { addCity })(AddCityForm);